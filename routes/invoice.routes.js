const router = require( "express" ).Router();
const Invoice = require( "../models/invoice.models" );
const invoiceControllers = require( "../controllers/invoice.controllers" );
const verifyToken = require( "../utils/verifyToken" );

router.param( "invoice", async( req, res, next, id )=>{
	try {
		const invoice = await Invoice.findById( id );
		if ( !invoice ) return res.sendStatus( 404 );
		req.invoice = invoice;
		return next();
	} catch ( err ) {
		res.status( 500 ).json( err );
	}
} );


router.get( "/", verifyToken, invoiceControllers.getInvoices );
router.post( "/create", verifyToken, invoiceControllers.createInvoice );

module.exports = router;
