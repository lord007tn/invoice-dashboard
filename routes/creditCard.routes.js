const router = require( "express" ).Router();
const creditCardControllers = require( "../controllers/creditCard.controllers" );
const CreditCard = require( "../models/creditCard.models" );
const verifyToken = require( "../utils/verifyToken" );
router.param( "creditCard", async( req, res,next, id )=>{
	try{
		const creditCard = await CreditCard.findById( id );
		if( !creditCard ) return res.sendStatus( 404 );
		req.creditCard = creditCard;
		return next();

	}catch( err ){
		return res.status( 500 ).json( { err_message: err } );
	}
} );
router.get( "/:creditCard", verifyToken, creditCardControllers.getCreditCard );
router.get( "/", verifyToken, creditCardControllers.getCreditCards );
router.post( "/create", verifyToken,creditCardControllers.createCreditCard );
router.put( "/:creditCard/update", verifyToken, creditCardControllers.updateCreditCard );
router.delete( "/:creditCard/delete", verifyToken, creditCardControllers.deleteCreditCard );

module.exports = router;