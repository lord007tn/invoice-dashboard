const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;

const CigarettesCountSchema = new Schema( {
	begin: { type: Date },
	end: { type: Date },
	added: { type: Number },
	sold: { type: Number }
}, { timestamps: true } );

module.exports = mongoose.model( "CigarettesCount", CigarettesCountSchema ) || mongoose.models.CigarettesCount;