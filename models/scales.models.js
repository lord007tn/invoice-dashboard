const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;

const ScalesSchema = new Schema( {
	axlesWeightTicketStart: { type: Date },
	axlesWeightTicketEnd: { type: Date },
	firstWeight: { type: Number },
	reWeight: { type: Number }
}, { timestamps: true } );

module.exports = mongoose.model( "Scales", ScalesSchema ) || mongoose.models.Scales;