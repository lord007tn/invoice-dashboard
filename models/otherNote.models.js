const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;

const OtherNoteSchema = new Schema( {
	note: { type: String },
}, { timestamps: true } );

module.exports = mongoose.model( "OtherNote", OtherNoteSchema ) || mongoose.models.OtherNote;