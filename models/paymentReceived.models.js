const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;

const PaymentReceivedSchema = new Schema( {
	name: { type: String },
	amount: { type: Number }
}, { timestamps: true } );

module.exports = mongoose.model( "PaymentReceived", PaymentReceivedSchema ) || mongoose.models.PaymentReceived;