const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;

const InvoiceSchema = new Schema( {
	date: { type: Date },
	cashier:{ type: mongoose.Schema.Types.ObjectId, ref: "User" },
	shift: { type: Number },
	pos: { type: Number },
	safeDrops: [ { type: Number } ],
	totalSafeDrops:{ type: Number, default: 0 },
	creditCards: [ {
		creditCard:{ type: mongoose.Schema.Types.ObjectId, ref: "CreditCard" },
		amount: { type: Number }
	} ],
	refunds: { type:Number },
	cashPaidOut: [ { type: Number } ],
	total:{ type: Number, default: 0 },
	tendersTotal:{ type: Number, default:0 },
	state:{ type: Number, default: 3 }, // 1 over, 2 short, 3
	stateCash: { type: Number, default: 0 },
	paymentReceived: [ { type: mongoose.Schema.Types.ObjectId, ref: "PaymentReceived" } ],
	certifiedScales: { type: mongoose.Schema.Types.ObjectId, ref: "CertifiedScales" },
	scales: { type: mongoose.Schema.Types.ObjectId, ref: "Scales" },
	cigarettesCount: { type: mongoose.Schema.Types.ObjectId, ref: "CigarettesCount" },
	otherNotes: [ { type: mongoose.Schema.Types.ObjectId, ref: "OtherNote" } ]
}, { timestamps: true } );

InvoiceSchema.pre( "save", function( next ){

	this.calculateTotalSafeDrops();
	this.calculateTotal();
	this.setState();
	this.calculateStateCash();
	next();
} );

InvoiceSchema.methods.calculateTotalSafeDrops = function(){
	for ( let i = 0; i < this.safeDrops.length; i++ ) {
		this.totalSafeDrops = this.safeDrops[i] + this.totalSafeDrops;
	}
};
InvoiceSchema.methods.calculateTotal = function(){
	for ( let i = 0; i < this.creditCards.length; i++ ) {
		this.total = this.creditCards[i].amount + this.total ;
	}
	for (let i = 0; i < this.cashPaidOut.length; i++) {
		this.total = this.total + this.cashPaidOut[i];	
	}
	this.total = this.total - this.refunds
};
InvoiceSchema.methods.setState = function(){
	if( this.total >= this.tendersTotal ){
		this.state = 1;
	}else{
		this.state = 2;
	}
};

InvoiceSchema.methods.calculateStateCash = function(){

	this.stateCash = this.total - this.tendersTotal ;
};
module.exports = mongoose.model( "Invoice", InvoiceSchema ) || mongoose.models.Invoice;