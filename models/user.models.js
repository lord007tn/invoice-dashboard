const mongoose = require( "mongoose" );
const uniqueValidator = require( "mongoose-unique-validator" );
const Schema = mongoose.Schema;

const UserSchema = new Schema( {
	firstName: { type: String, max: 64 },
	lastName: { type:String, max: 64 },
	email: { type: String, required:[ true, "can't be blank" ], index:true, lowercase: true, unique: true, },
	password: { type: String, required: [ true, "can't be blank" ], max: 1024 },
	isAdmin: { type: Boolean, default: false },
}, { timestamps: true } );

UserSchema.plugin( uniqueValidator, { message: "is already taken." } );

UserSchema.methods.authToJSON = function(){
	return {
		_id: this._id,
		email: this.email,
		firstName: this.firstName,
		lastName: this.lastName,
		isAdmin: this.isAdmin
	};
};

module.exports = mongoose.model( "User", UserSchema ) || mongoose.models.User;