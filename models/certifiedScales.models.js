const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;

const CertifiedScalesSchema = new Schema( {
	certifiedTicketStart: { type: Date },
	certifiedTicketEnd: { type: Date },
	totalCertifiedScales: { type: Number }
}, { timestamps: true } );

module.exports = mongoose.model( "CertifiedScales", CertifiedScalesSchema ) || mongoose.models.CertifiedScales;