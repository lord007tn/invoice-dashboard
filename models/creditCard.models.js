const uniqueValidator = require( "mongoose-unique-validator" );
const mongoose = require( "mongoose" );
const Schema = mongoose.Schema;

const CreditCardSchema = new Schema( {
	name: { type: String, unique:true, index:true },
}, { timestamps: true } );

CreditCardSchema.plugin( uniqueValidator, { message: "is already taken." } );

module.exports = mongoose.model( "CreditCard", CreditCardSchema ) || mongoose.models.CreditCard;