const path = require( "path" );
const express = require( "express" );
const app = express();
const bodyParser = require( "body-parser" );
const mongoose = require( "mongoose" );
const dotenv = require( "dotenv" );
const morgan = require( "morgan" );
const helmet = require( "helmet" );
const cors = require( "cors" );
//Dotenv config
dotenv.config();
//Connect to database
mongoose.set( "useNewUrlParser", true );
mongoose.set( "useFindAndModify", false );
mongoose.set( "useCreateIndex", true );
mongoose.set( "useUnifiedTopology", true );
mongoose.connect( "mongodb+srv://safwen:123456@psw@cluster0.pvimw.mongodb.net/invoice?retryWrites=true&w=majority" );
//mongoose.connect( process.env.MONGO_DB );
mongoose.connection.on( "connected", () => {console.log( "DB Connected" );} );
mongoose.connection.on( "error", ( err ) => console.log( "DB Connection failed with - ", err ) );
//Import routes
const authRoutes = require( "./routes/auth.routes" );
const creditCardRoutes = require( "./routes/creditCard.routes" );
const invoiceRoutes = require( "./routes/invoice.routes" );
//Middleware
app.use( helmet() );
app.use( cors() );
app.use( bodyParser.json() );
app.use( bodyParser.urlencoded( { extended : false } ) );
app.use( morgan( "dev" ) );

//Routes middleware
app.use( "/api", authRoutes );
app.use( "/api/creditcards", creditCardRoutes );
app.use( "/api/invoice", invoiceRoutes );
// static file
if( process.env.NODE_ENV === "production" ){
	app.use( express.static( "/client/build" ) );

	app.get( "*", ( req, res ) => {
		res.sendFile( path.resolve( __dirname, "client", "build", "index.html" ) );
	} );
	
}
//Server run
const port = 8000 || process.env.PORT;
app.listen( port, ()=>{
	console.log( "Server is up and running on port number " + port );
} );