import axios from 'axios';
import {
    INVOICE_ERROR,
    GET_INVOICE,
    GET_INVOICES,
    CREATE_INVOICE
} from './types'

export const createInvoice = (formData) => async dispatch =>{

    const config ={
        headers:{
            'Content-Type': 'application/json'
        }
    }
    const invoice = JSON.stringify(formData)
    try {
        const res = await axios.post('/api/invoice/create', invoice, config)
        dispatch({
            type: CREATE_INVOICE,
            payload: res.data
        })
    } catch (err) {
        dispatch({
            type: INVOICE_ERROR,
            payload: err
        })
    }
}

export const getInvoices = () => async dispatch => {
    try {
        const res = await axios.get('/api/invoice/')
        dispatch({
            type: GET_INVOICES,
            payload: res.data.invoices
        })
    } catch (err) {
        dispatch({
            type: INVOICE_ERROR,
            payload: err
        })
    }
}