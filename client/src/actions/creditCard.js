import axios from 'axios';
import {
    CREDIT_CARD_ERROR,
    GET_CREDIT_CARD,
    GET_CREDIT_CARDS
} from './types'

export const getCreditCards = () => async dispatch => {
    try {
        const res = await axios.get('/api/creditcards/')
        dispatch({
            type: GET_CREDIT_CARDS,
            payload: res.data.creditCards
        })
    } catch (err) {
        dispatch({
            type: CREDIT_CARD_ERROR,
            payload: err
        })
    }
}