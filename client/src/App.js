import React, { Fragment, useEffect } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import './App.css';

import setAuthToken from './utils/setAuthToken'
import { loadUser } from './actions/auth'
//Redux
import { Provider } from 'react-redux'
import store from './store'
import Register from './components/auth/Register';
import Login from './components/auth/Login';
import Routes from './components/routing/Routes';


if (localStorage.token) {
  setAuthToken(localStorage.token)
}

const App = () => {
  useEffect(() => {
    store.dispatch(loadUser())
  }, [])
  return (
    <Provider store={store}>
      <Router>
        <Fragment>
          <Switch>
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route component={Routes} />
          </Switch>
        </Fragment>
      </Router>
    </Provider>

  );
}

export default App;
