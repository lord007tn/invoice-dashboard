import {combineReducers} from 'redux';
import auth from './auth'
import creditCard from './creditCard'
import invoice from './invoice'
export default combineReducers({
    auth,
    creditCard,
    invoice
})