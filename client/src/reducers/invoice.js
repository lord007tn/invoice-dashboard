import {
    CREATE_INVOICE,
    GET_INVOICE,
    GET_INVOICES,
    INVOICE_ERROR
} from '../actions/types';

const initialState = {
    invoices: [],
    invoice: null,
    loading: true,
    error: {}
}

export default function(state= initialState, action){

    const {type, payload} = action
    switch (type) {
        case CREATE_INVOICE:
            return{
                ...state,
                invoices: [payload, ...state.invoices],
                loading: false
            }
        case GET_INVOICES:
            return{
                ...state,
                invoices: payload,
                loading: false
            }
        case GET_INVOICE:
            return{
                ...state,
                invoice: payload,
                loading: false
            }
        case INVOICE_ERROR:
            return{
                ...state,
                error: payload,
                loading:false
            }
        default:
            return state;
    }
}