import {
    CREDIT_CARD_ERROR,
    GET_CREDIT_CARD,
    GET_CREDIT_CARDS
} from '../actions/types'
const initialState = {
    creditCards : [],
    creditCard: null,
    loading: true,
    error: {}
}

export default function(state = initialState, action){
    const {type, payload} = action
    switch (type) {
        case GET_CREDIT_CARDS:
            return{
                ...state,
                creditCards: payload,
                loading: false
            }
        case GET_CREDIT_CARD:
            return{
                ...state,
                creditCard: payload,
                loading: false
            }
        case CREDIT_CARD_ERROR:
            return{
                ...state,
                error: payload,
                loading: false
            }
        default:
            return state;
    }

}