import React, {Fragment, useState} from 'react'
import PropTypes from 'prop-types'
import {Redirect, Link} from 'react-router-dom';
import Toggler from './Toggler';
import SideNav from './SideNav';
const SideBar = props => {
    const [sideBarOpen, setsideBarOpen] = useState(true);
    const openHandler = () => {
        if (!sideBarOpen){
            setsideBarOpen(true)
        }else{
            setsideBarOpen(false)
        }
    }
    let sideNav;
    if (sideBarOpen){
        sideNav= (
            <Fragment>
        <div className="close-layer visible transform-hide" onClick={openHandler}></div>
        <SideNav/>
            </Fragment>

            )
    }
    return (
        <Fragment>
        {sideNav}
        <Toggler click={openHandler}/>
        </Fragment>
        
    )
}

SideBar.propTypes = {

}

export default SideBar
