import React, { Fragment } from 'react'
import PropTypes from 'prop-types'
import { Redirect, Link } from 'react-router-dom';
const SideNav = props => {
  return (
    <Fragment>

      <div className="sidebar" data-color="purple" data-background-color="black">
        <div className="logo"><a className="simple-text logo-normal">
          Invoice Platform
        </a></div>
        <div className="sidebar-wrapper">
          <ul className="nav">
            <li className="nav-item hover-active">
              <Link className="nav-link" to="/dashboard/home">
                <i className="material-icons">dashboard</i>
                <p>Dashboard</p>
              </Link>
            </li>
            <li className="nav-item hover-active">
              <Link className="nav-link" to="/dashboard/invoice">
                <i className="material-icons">receipt</i>
                <p>Invoice</p>
              </Link>
            </li>
          </ul>
        </div>
      </div>

    </Fragment>
  )
}

SideNav.propTypes = {

}

export default SideNav
