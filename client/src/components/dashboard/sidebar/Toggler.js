import React, {Fragment} from 'react'
import PropTypes from 'prop-types'

const Toggler = props => {
    return (
        <Fragment>
        <nav className="navbar navbar-absolute">
        <div className="container-fluid">
        <div className="navbar-wrapper">
        <button className="navbar-toggler" type="button" onClick={props.click}>
        <span className="material-icons">menu</span>
          </button>
        </div>
        </div>
        </nav>


        </Fragment>
    )
}

export default Toggler

