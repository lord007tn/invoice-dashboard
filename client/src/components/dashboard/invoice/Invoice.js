import React, { Fragment, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';
import {connect} from 'react-redux'
import Spinner from '../../layout/Spinner'
import { getInvoices } from '../../../actions/invoice'
import moment from 'moment';
const Invoice = ({getInvoices, auth:{loading, user}, invoice}) => {
  useEffect(() => {
    getInvoices()
  }, [getInvoices]);
  return (loading && invoice.loading ? (<div className="disableAllWithSpin">
  <div className="centerSpin">
      <Spinner />
  </div>
  </div>) :
    <Fragment>
      <div>
        <div className="col-md-3 my-3">
          <div className="row">

            <Link className="btn btn-primary" to="/dashboard/newinvoice">
             New Invoice
            </Link>
          </div>
        </div>

        <div className="row">
          <div className="col-md-12">
            <div className="card">
              <div className="card-header card-header-primary">
                <h4 className="card-title">Invoices</h4>
              </div>
              <div className="card-body table-responsive">
                <table className="table table-hover">
                  <thead className="text-warning">
                    <tr>
                      <th>#</th>
                      <th>Date</th>
                      <th>Total Cash</th>
                      <th>State</th>
                      <th>State Cash</th>
                      <th>Shift</th>
                      <th>POS</th>
                    </tr>
                  </thead>
                  <tbody>
                  {invoice && invoice.invoices.map((invoice, i) =>{
                    return (
                    <tr key={invoice._id}>
                      <td>{i+1}</td>
                      <td>{moment(invoice.date).format('YYYY-MM-DD')}</td>
                      <td>{invoice.total}</td>
                      <td>{invoice.state === 1 ? "over": "short"}</td>
                      <td>{invoice.stateCash}</td>
                      <td>{invoice.shift}</td>
                      <td>{invoice.pos}</td>
                    </tr>
                    )}
                    ) 
                    }

                  </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
      </div>
    </Fragment>
  )
}

Invoice.propTypes = {
  auth: PropTypes.object.isRequired,
  invoice: PropTypes.object.isRequired,
  getInvoices: PropTypes.func.isRequired,
}

const mapStateToProps = state => ({
  auth:state.auth,
  invoice: state.invoice

})
export default connect(mapStateToProps, {getInvoices})(Invoice)
