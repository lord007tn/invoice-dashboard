import React, { Fragment, useState, useEffect } from 'react'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import {getCreditCards} from '../../../actions/creditCard'
import Spinner from '../../layout/Spinner'
import moment from 'moment';
import { createInvoice, getInvoices } from '../../../actions/invoice'
const NewInvoice = ({getCreditCards, createInvoice, getInvoices, invoice, auth:{user, loading}, creditCardState}) => {

    useEffect(() => {
        getCreditCards()
        getInvoices()
    }, [getCreditCards, getInvoices]);
    const [CreditCardList, setCreditCardList] = useState([])
    useEffect(()=>{
        getCreditCards();
        setCreditCardList(creditCardState.loading || !creditCardState.creditCards ? [] : creditCardState.creditCards)
    }, [creditCardState.loading])

    useEffect(()=>{
        setFormData({
            cashier: loading || !user._id ? "" : user._id
        })
    }, [loading])
    const [FormData, setFormData] = useState({date: "",cashier:"",shift: 1,pos:0,refunds: 0})
    const [CigarettesCount, setCigarettesCount] = useState({begin:"", end:"", added:0, sold:0})
    const [Scales, setScales] = useState({axlesWeightTicketStart:"", axlesWeightTicketEnd:"", firstWeight:0, reWeight:0})
    const [CertifiedScales, setCertifiedScales] = useState({certifiedTicketStart:"", certifiedTicketEnd:"", totalCertifiedScales:1});
    const [OtherNotes, setOtherNotes] = useState([{note:""}])
    const [PaymentsReceived, setPaymentsReceived] = useState([{name:"", amount:0}]);
    const [CreditCards, setCreditCards] = useState([{creditCard: "", amount: 0}]);
    const [CashPaidOut, setCashPaidOut] = useState([0])
    const [SafeDrops, setSafeDrops] = useState([0]);

    const {date,shift,pos,refunds} = FormData
    const scales = {...Scales}
    const certifiedScales = {...CertifiedScales}
    const cigarettesCount = {...CigarettesCount}
    const onChange = e =>{
        setFormData({...FormData, [e.target.name]: e.target.value})
    }

    const onChangeCertifiedScales = e => {
        setCertifiedScales({...certifiedScales, [e.target.name]: e.target.value})
    }
    const onChangeScales = e => {
        setScales({...scales, [e.target.name]: e.target.value})
    }
    const onChangeCigarettesCount = e =>{
        setCigarettesCount({...cigarettesCount, [e.target.name]: e.target.value})
    }
const onChangeOtherNotes = (e, index) => {
    const { name, value } = e.target;
    const otherNote = [...OtherNotes]
    otherNote[index][name] = value;
    setOtherNotes(otherNote)
}
const otherNotesAddClick = (i) => {
    if(OtherNotes[i].note !== ""){
        setOtherNotes([
            ...OtherNotes, {note: ""}
        ])
    }
  };
const onChangePaymentsReceived = (e, index) => {
    const { name, value } = e.target;
    const payments = [...PaymentsReceived]
    payments[index][name] = value;
    setPaymentsReceived(payments)
}
const PaymentsReceivedAddClick = (i) => {
    if((PaymentsReceived[i].name !== "") && (PaymentsReceived[i].amount >= 0)){
        setPaymentsReceived([
            ...PaymentsReceived, {name: "", amount: 0}
        ])
    }
  };
  const onChangeCreditCards = (e, index) => {
    const { name, value } = e.target;
    const cards = [...CreditCards]
    cards[index][name] = value;
    setCreditCards(cards)
}
const CreditCardsAddClick = (i) => {
    if((CreditCards[i].creditCard !== "") && (CreditCards[i].amount > 0)){
        setCreditCards([
            ...CreditCards, {creditCard: "", amount: 0}
        ])
    }
  };
  const onChangeCashPaidOut = (e, index) => {
    const { value } = e.target;
    const cash = [...CashPaidOut]
    cash[index] = Number(value);
    setCashPaidOut(cash)
}
const CashPaidOutAddClick = (i) => {
    if(CashPaidOut[i] > 0){
        setCashPaidOut([...CashPaidOut, []])
    }
  };
  const onChangeSafeDrops = (e, index) => {
    const { value } = e.target;
    const drops = [...SafeDrops]
    drops[index] = Number(value);
    setSafeDrops(drops)
}
const SafeDropsAddClick = (i) => {
    if(SafeDrops[i] > 0){
        setSafeDrops([...SafeDrops, []])
    }
  };

let totalSafeDrops = 0
const calculateTotalSafeDrops = () =>{
    for (let i = 0; i < SafeDrops.length; i++) {
       totalSafeDrops += SafeDrops[i];
    }
    return totalSafeDrops
}
let total = 0
const calculateTotal = () =>{
    let totalCreditCard = 0
    for (let i = 0; i < CreditCards.length; i++) {
        totalCreditCard += Number(CreditCards[i].amount);
    }
    let totalCashPaidOut = 0
    for (let i = 0; i < CashPaidOut.length; i++) {
        totalCashPaidOut += Number(CashPaidOut[i]);
    }
    total = total + Number(totalCashPaidOut) + Number(totalCreditCard) - refunds
    return total
}

let state = 0
const calculateState = () =>{
    return state = total - 0
}
  const onSubmit = e =>{
    e.preventDefault()
    createInvoice({FormData,
        certifiedScales: CertifiedScales,
        scales: Scales,
        cigarettesCount: CigarettesCount,
        otherNotes: OtherNotes,
        paymentsReceived: PaymentsReceived,
        creditCards: CreditCards,
        cashPaidOut: CashPaidOut,
        safeDrops: SafeDrops })
    setFormData({date: "",cashier:"",shift: 1,pos:0,refunds: 0})
    setCigarettesCount({begin:"", end:"", added:0, sold:0})
    setScales({axlesWeightTicketStart:"", axlesWeightTicketEnd:"", firstWeight:0, reWeight:0})
    setCertifiedScales({certifiedTicketStart:"", certifiedTicketEnd:"", totalCertifiedScales:1})
    setOtherNotes([{note:""}])
    setPaymentsReceived([{name:"", amount:0}])
    setCreditCards([{creditCard: "", amount: 0}])
    setCashPaidOut([0])
    setSafeDrops([0])
}
    return (loading && creditCardState.loading && invoice.loading ? (<div className="disableAllWithSpin">
    <div className="centerSpin">
        <Spinner />
    </div>
    </div>) :(
        <Fragment>
            <div>
                <div className="marginRow">
                <div className="col-md-6 offset-md-3 mx-auto col-sm-auto">
                        <div className="card">
                            <div className="card-body">
                                <form>
                                <div className="row">
                                    <div className="col-sm">
                                            <label className="bmd-label-floating">Date:</label>
                                        </div>
                                    <div className="col-sm">
                                        <input type="date" name="date" value= {date || moment().format('YYYY-MM-DD')} onChange={e => onChange(e)}  placeholder="Date" required className="form-control"/>
                                        </div>
                                    </div>
                                    <div className="row">
                                    <div className="col-sm">
                                            <label className="bmd-label-floating">Shift: </label>
                                        </div>
                                    <div className="col-sm">
                                        <input type="number" min="1" name="shift" value= {shift || 1} onChange={e => onChange(e)}  placeholder="Shift" required className="form-control" />
                                        </div>
                                    </div>
                                    <div className="row">
                                    <div className="col-sm">
                                            <label className="bmd-label-floating">POS:</label>
                                        </div>
                                    <div className="col-sm">
                                        <input type="number" min="1" name="pos" value= {pos || 0} onChange={e => onChange(e)}  placeholder="POS" required className="form-control" />
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                    <div className="col-md-6 col-sm-auto">
                        <div className="card">
                            <div className="card-header card-header-primary">
                                <h4 className="card-title">IN HAND</h4>
                            </div>
                            <div className="card-body">
                                <form>
                                {SafeDrops.map((x, i)=>{
                                    return (
                                        <div className="row" key={i}>
                                        <div className="col-md-5 col-sm-auto">
                                            <label className="text-nowrap bd-highlight">Safe Drops:</label>
                                        </div>
                                        <div className="col-md-5 col-sm-auto">
                                            <div className="form-group">
                                                    <input type="number" min="0" name="safeDrops" key={i} value={x} onChange={e => onChangeSafeDrops(e, i)} placeholder="Cash" className="form-control"/>
                                            </div>
                                        </div>
                                        {SafeDrops.length -1 === i && <button type="button" onClick={() => SafeDropsAddClick(i)} className="btn btn-primary rounded-pill">+</button>}
                                    </div>
                                    )
                                })}

                                    <div className="row">
                                        <div className="col-md-3 col-sm-auto">
                                            <label className="text-nowrap bd-highlight">Total Safe Drops:</label>
                                        </div>
                                        <div className="col-md-5 col-sm-auto"></div>
                                        <div className="col-md-3 col-sm-auto">
                                            <label className="text-nowrap bd-highlight text-right  align-self-end">{calculateTotalSafeDrops() || 0} $</label>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-sm-auto">
                        <div className="card">
                            <div className="card-header card-header-primary">
                                <h4 className="card-title">PAYMENTS RECEIVED</h4>
                            </div>
                            <div className="card-body">
                            {PaymentsReceived.map((x, i)=>{
                                return(
                                    <form key={i}>
                                    <div className="row">
                                        <div className="col-md-5 col-sm-auto">
                                            <div className="form-group">
                                                <input type="text" name="name" key={i} value={x.name} onChange={e => onChangePaymentsReceived(e, i)} placeholder="Name" className="form-control" />
                                            </div>
                                        </div>
                                        <div className="col-md-5 col-sm-auto">
                                            <div className="form-group">
                                                <input type="number" min="0" name="amount" key={i} value= {x.amount} onChange={e => onChangePaymentsReceived(e, i)}  placeholder="Amount" className="form-control" />
                                            </div>
                                        </div>
                                        {PaymentsReceived.length -1 === i && <button type="button" onClick={() => PaymentsReceivedAddClick(i)} className="btn btn-primary rounded-pill">+</button>}
                                    </div>
                                </form>
                                )
                            })}

                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div className="row marginRow">
                    <div className="col-md-6 col-sm-auto">
                        <div className="card">

                            <div className="card-body">
                                <form>
                                {CreditCards.map((x, i)=>{
                                    return (
                                        <div className="row" key={i}>
                                        <div className="col-md-5 col-sm-auto">
                                            <div className="form-group">
                                                <select onChange={e => onChangeCreditCards(e, i)} key={i} name="creditCard" placeholder="" className="form-control selectpicker" style={{ backgroundColor: "#1A2035", borderRadius: 7, paddingRight: 7, paddingLeft: 7 }}>
                                                    <option value="">Choose credit card</option>
                                                    {CreditCardList.map((x, i)=>{
                                                        return (
                                                            <option key={i} value={x._id}>{x.name}</option>
                                                        )
                                                    })}
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-md-5 col-sm-auto">
                                            <div className="form-group">
                                                <input type="number" min="0" name="amount" value= {x.amount} onChange={e => onChangeCreditCards(e, i)}  placeholder="Amount" className="form-control" />
                                            </div>

                                        </div>
                                        {CreditCards.length -1 === i && <button type="button" onClick={() => CreditCardsAddClick(i)} className="btn btn-primary rounded-pill">+</button>}
                                    </div>
                                    )
                                })}

                                    <div className="row my-2 ">
                                        <div className="col-md-5 col-sm-auto">
                                            <label className="text-nowrap bd-highlight">Refunds:</label>
                                        </div>
                                        <div className="col-md-6 col-sm-auto">
                                            <input type="number" name="refunds" value= {refunds || 0} onChange={e => onChange(e)}  placeholder="Refunds" className="form-control" />
                                        </div>
                                    </div>
                                    {CashPaidOut.map((x, i)=>{
                                        return(
                                    <div className="row my-2" key={i}>
                                        <div className="col-md-5 col-sm-auto">
                                            <label className="text-nowrap bd-highlight">Cash Paid-out:</label>
                                        </div>
                                        <div className="col-md-5 col-sm-auto">
                                            <input type="number" min="0" name="cashPaidOut" key={i} value={x} onChange={e => onChangeCashPaidOut(e, i)} placeholder="Cash" className="form-control" />
                                        </div>
                                        {CashPaidOut.length -1 === i && <button type="button" onClick={() => CashPaidOutAddClick(i)} className="btn btn-primary rounded-pill">+</button>}
                                    </div>
                                        )
                                    })}

                                    <div className="row mt-3">
                                        <div className="col-md-8 col-sm-auto">
                                            <label className="text-nowrap bd-highlight"> Total:</label>
                                        </div>

                                        <div className="col-md-3 col-sm-auto">
                                            <label className="text-nowrap bd-highlight">{calculateTotal() || 0} $</label>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6 col-sm-auto">
                        <div className="card">
                            <div className="card-header card-header-primary">
                                <h4 className="card-title">SCALES</h4>
                            </div>
                            <div className="card-body">
                                <form>
                                    <div className="row">
                                        <div className="col-md-7 col-sm-auto">
                                            <div className="form-group">
                                                <label className="bmd-label-floating">Axles weight ticket start:</label>
                                            </div>
                                        </div>
                                        <div className="col-md-5 col-sm-auto">
                                            <div className="form-group">
                                                <input type="date" name="axlesWeightTicketStart" value= {scales.axlesWeightTicketStart || moment().format('YYYY-MM-DD')} onChange={e => onChangeScales(e)}  placeholder="Axles Weight Ticket Start" className="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-7 col-sm-auto">
                                            <div className="form-group">
                                                <label className="bmd-label-floating">Axles weight ticket end:</label>
                                            </div>
                                        </div>
                                        <div className="col-md-5 col-sm-auto">
                                            <div className="form-group">
                                                <input type="date" name="axlesWeightTicketEnd" value= {scales.axlesWeightTicketEnd || moment().format('YYYY-MM-DD')} onChange={e => onChangeScales(e)}  placeholder="Axles Weight Ticket End" className="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-7 col-sm-auto">
                                            <b>Total Scales:</b>
                                            </div>
                                    </div>
                                    <div className="row">
                                    <div className="col">
                                    <div className="form-group">
                                    <label className="bmd-label-floating">First Weight</label>
                                        <input type="number" min="0" name="firstWeight" value= {scales.firstWeight || 0} onChange={e => onChangeScales(e)}  placeholder="First Weight" className="form-control" />
                                    </div>
                                    </div>
                                    <div className="col">
                                    <div className="form-group">
                                    <label className="bmd-label-floating">Re-Weight</label>
                                        <input type="number" min="0" name="reWeight" value= {scales.reWeight || 0} onChange={e => onChangeScales(e)}  placeholder="Re-Weight" className="form-control" />
                                    </div>
                                    </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                <div className="col-md-6 col-sm-auto">
                        <div className="card">

                            <div className="card-body">
                                <div className="row">
                                    <div className="col-md-8 col-sm-auto">
                                        <b>Tenders Total:</b>
                                    </div>
                                    <div className="col-md-4 col-sm-auto">
                                        <b>00000000 $</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                <div className="col-md-6 col-sm-auto">
                        <div className="card">

                            <div className="card-body">
                                <div className="row">
                                    <div className="col-md-8 col-sm-auto">
                                        <b>Over/Short:</b>
                                    </div>
                                    <div className="col-md-4 col-sm-auto">
                                        <b className={`${calculateState() > 0 ? "text-danger": "text-success"}`}>{calculateState() || 0} $</b>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
                </div>
                    <div className="row">
                    <div className="col-md-6 col-sm-auto">
                        <div className="card">
                        <div className="card-header card-header-primary">
                                <h4 className="card-title">CERTIFIED TICKET</h4>
                            </div>
                            <div className="card-body">
                                <form>
                                    <div className="row my-2">
                                        <div className="col-md-5 col-sm-auto">
                                            <label className="text-nowrap bd-highlight">Certified ticket start:</label>
                                        </div>
                                        <div className="col-md-6 col-sm-auto">
                                            <input type="date" name="certifiedTicketStart" value= {certifiedScales.certifiedTicketStart || moment().format('YYYY-MM-DD')} onChange={e => onChangeCertifiedScales(e)}  placeholder="Certified Ticket Start" className="form-control" />
                                        </div>
                                    </div>
                                    <div className="row mt-2">
                                        <div className="col-md-5 col-sm-auto">
                                            <label className="text-nowrap bd-highlight">Certified ticket end:</label>
                                        </div>
                                        <div className="col-md-6 col-sm-auto">
                                            <input type="date" name="certifiedTicketEnd" value= {certifiedScales.certifiedTicketEnd || moment().format('YYYY-MM-DD')} onChange={e => onChangeCertifiedScales(e)}  placeholder="Certified Ticket End" className="form-control" />
                                        </div>
                                    </div>
                                    <div className="row mt-3">
                                        <div className="col-md-8 col-sm-auto">
                                            <label className="text-nowrap bd-highlight"> Total Certified Scales:</label>
                                        </div>
                                        <div className="col-md-3 col-sm-auto">
                                        <input type="number" min="0" name="totalCertifiedScales" value= {certifiedScales.totalCertifiedScales || 0} onChange={e => onChangeCertifiedScales(e)}  placeholder="Total Certified Scales" className="form-control" />
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="card">
                            <div className="card-header card-header-primary">
                                <h4 className="card-title">CIGARETTES COUNT</h4>
                            </div>
                            <div className="card-body">
                                <form>
                                    <div className="row">
                                        <div className="col-md-7 col-sm-auto">
                                            <div className="form-group">
                                                <label className="bmd-label-floating">Begin:</label>
                                            </div>
                                        </div>
                                        <div className="col-md-5 col-sm-auto">
                                            <div className="form-group">
                                                <input type="date" name="begin" value= {cigarettesCount.begin || moment().format('YYYY-MM-DD')} onChange={e => onChangeCigarettesCount(e)}  placeholder="Begin" className="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-7 col-sm-auto">
                                            <div className="form-group">
                                                <label className="bmd-label-floating">End:</label>
                                            </div>
                                        </div>
                                        <div className="col-md-5 col-sm-auto">
                                            <div className="form-group">
                                                <input type="date" name="end" value= {cigarettesCount.end || moment().format('YYYY-MM-DD')} onChange={e => onChangeCigarettesCount(e)}  placeholder="End" className="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-7 col-sm-auto">
                                            <div className="form-group">
                                                <label className="bmd-label-floating">Added:</label>
                                            </div>
                                        </div>
                                        <div className="col-md-5 col-sm-auto">
                                            <div className="form-group">
                                                <input type="number" min="0" name="added" value= {cigarettesCount.added || 0} onChange={e => onChangeCigarettesCount(e)}  placeholder="Added" className="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-md-7 col-sm-auto">
                                            <div className="form-group">
                                                <label className="bmd-label-floating">Sold:</label>
                                            </div>
                                        </div>
                                        <div className="col-md-5 col-sm-auto">
                                            <div className="form-group">
                                                <input type="number" min="0" name="sold" value= {cigarettesCount.sold || 0} onChange={e => onChangeCigarettesCount(e)}  placeholder="Sold" className="form-control" />
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    </div>

            <div className="row">
                <div className="col-md-6 col-sm-auto">
                        <div className="card">
                            <div className="card-header card-header-primary">
                                <h4 className="card-title"> OTHER NOTES</h4>
                            </div>
                            <div className="card-body">
                                {OtherNotes.map((x, i) => {
                                return(
                                    <form key={i}>
                                    <div className="row">

                                        <div className="col-md-10 col-sm-auto">
                                            <div className="form-group">
                                                <textarea type="textarea" name="note" key={i} value={x.note} onChange={e => onChangeOtherNotes(e, i)} placeholder="Note" className="form-control" />
                                            </div>
                                        </div>
                                        {OtherNotes.length -1 === i && <button type="button" onClick={() => otherNotesAddClick(i)} className="btn btn-primary rounded-pill">+</button>}
                                    </div>
                                </form>
                                )
                            })}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row offset-6">
                <button type="button" onClick={ e=> onSubmit(e)} className="btn btn-primary">Create</button>
                </div>
            </div>
        </Fragment>)
    )
}
NewInvoice.propTypes = {
    auth: PropTypes.object.isRequired,
    creditCardState: PropTypes.object.isRequired,
    invoice: PropTypes.object.isRequired,
    getCreditCards: PropTypes.func.isRequired,
    createInvoice: PropTypes.func.isRequired,
    getInvoices: PropTypes.func.isRequired,
}
const mapStateToProps = state => ({
    auth:state.auth,
    creditCardState: state.creditCard,
    invoice: state.invoice

})
export default connect(mapStateToProps, {getCreditCards, createInvoice, getInvoices})(NewInvoice);
