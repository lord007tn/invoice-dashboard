import React,{useState, Fragment} from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {login} from '../../actions/auth'
import {Link, Redirect} from 'react-router-dom'

import Spinner from '../layout/Spinner'
const Login = ({login, isAuthenticated, loading}) => {
    const [FormData, setFormData] = useState({
        email:"",
        password:""
    });
    const{email, password} = FormData;
    const onChange = e => {
        setFormData({...FormData, [e.target.name]: e.target.value})
    }
    const onSubmit = e => {
        e.preventDefault()
        login(email, password);
        setFormData({email:"", password:""})
    }
    if (isAuthenticated){
        return <Redirect to="/dashboard/home"/>
    }
    return (
        loading ? (<div className="disableAllWithSpin">
    <div className="centerSpin">
        <Spinner />
    </div>
    </div>) :(
        <Fragment>
<div className="login-dark">
        <form onSubmit={e => onSubmit(e)}>
            <h2 className="sr-only">Register Form</h2>
            <div className="illustration"><i className="icon ion-ios-locked-outline"></i></div>
            <div className="form-group"><input type="email" className="form-control" name="email" value= {email} onChange={e => onChange(e)}  placeholder="Email" required/></div>
            <div className="form-group"><input type="password" className="form-control" name="password" value= {password} onChange={e => onChange(e)}  placeholder="Password" required/></div>
            <div className="form-group"><button className="btn btn-primary btn-block" type="submit">Login</button></div>
            <Link to='/register' className="forgot">I'm new</Link>
            </form>
    </div>
        </Fragment>
    )

    )
}

Login.propTypes = {
    login: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool,
    loading: PropTypes.bool.isRequired,
}

const mapStateToProps = state =>({
    isAuthenticated: state.auth.isAuthenticated,
    loading: state.auth.loading
})

export default connect(mapStateToProps, {login})(Login)
