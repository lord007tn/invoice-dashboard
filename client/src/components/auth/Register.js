import React, {Fragment, useState} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';
import {register} from '../../actions/auth'
import { Link, Redirect } from 'react-router-dom';

import Spinner from '../layout/Spinner'
const Register = ({register, isAuthenticated, loading}) => { 
    const [FormData, setFormData] = useState({
        firstName: "",
        lastName: "",
        email: "",
        password: "",
        password2:""
    })

    const {firstName, lastName, email, password, password2} = FormData

    const onChange = e =>{
        setFormData({...FormData, [e.target.name]: e.target.value})
    }

    const onSubmit = e =>{
        e.preventDefault()
        if(password !== password2){
            setFormData({password:"", password2:""})
        }else{
             register({firstName, lastName, email, password});
             setFormData({firstName: e.target.firstName.value, lastName: e.target.lastName.value, email:"", password:"", password2:""})
        }
     }
     if(isAuthenticated){
         return <Redirect to="/dashboard/home"/>
     }
    return (loading ? (<div className="disableAllWithSpin">
    <div className="centerSpin">
        <Spinner />
    </div>
    </div>) : (
        <Fragment>
<div className="login-dark">
        <form onSubmit={e => onSubmit(e)}>
            <h2 className="sr-only">Register Form</h2>
            <div className="illustration"><i className="icon ion-ios-locked-outline"></i></div>
            <div className="form-group"><input type="text" className="form-control" name="firstName" value= {firstName} onChange={e => onChange(e)}  placeholder="First Name" required/></div>
            <div className="form-group"><input type="text" className="form-control" name="lastName" value= {lastName} onChange={e => onChange(e)}  placeholder="Last Name" required/></div>
            <div className="form-group"><input type="email" className="form-control" name="email" value= {email} onChange={e => onChange(e)}  placeholder="Email" required/></div>
            <div className="form-group"><input type="password" className="form-control" name="password" value= {password} onChange={e => onChange(e)}  placeholder="Password" required/></div>
            <div className="form-group"><input type="password" className="form-control" name="password2" value= {password2} onChange={e => onChange(e)}  placeholder="Confirm Password" required/></div>
            <div className="form-group"><button className="btn btn-primary btn-block" type="submit">Register</button></div>
            <Link to='/login' className="forgot">I have an account</Link>
            </form>
    </div>
        </Fragment>
    )

    )
}

Register.propTypes = {
    register: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool,
    loading: PropTypes.bool.isRequired,
}
const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated,
    loading: state.auth.loading
})

export default connect(mapStateToProps, {register})(Register)
