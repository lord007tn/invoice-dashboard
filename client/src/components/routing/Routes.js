import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Invoice from '../dashboard/invoice/Invoice'
import SideBar from '../dashboard/sidebar/SideBar';
import Home from '../dashboard/home/Home';
import NewInvoice from '../dashboard/newInvoice/NewInvoice';
import PrivateRoute from './PrivateRoute'
const Routes = () => {
    return (
        <div className="dark-edition">
        <div className="wrapper ">
            <SideBar />
            <div className="main-panel">
                        <div className="content">
                            <div className="container-fluid">
            <Switch>

                                <PrivateRoute exact path="/dashboard/home" component={Home} />
                                <PrivateRoute exact path="/dashboard/invoice" component={Invoice} />
                                <PrivateRoute exact path="/dashboard/newinvoice" component={NewInvoice} />

            </Switch>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        

    )
}

export default Routes
