const CreditCard = require( "../models/creditCard.models" );


const createCreditCard = async( req, res )=>{

	const creditCard = new CreditCard( {
		name: req.body.name,
	} );

	try {
		const savedCreditCard = await creditCard.save();
		return res.status( 200 ).json( { savedCreditCard: savedCreditCard } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const getCreditCard = async( req, res )=>{

	const creditCard = req.creditCard;

	try {
		return res.status( 200 ).json( { creditCard: creditCard } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const getCreditCards = async( req, res )=>{

	try {
		const creditCards = await CreditCard.find();
		return res.status( 200 ).json( { creditCards: creditCards } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const updateCreditCard = async( req , res )=> {

	const creditCard= req.creditCard;

	try {
		const dataToUpdate = req.body;
		const { ...updateData } = dataToUpdate;
		const updateCreditCard = await CreditCard.findByIdAndUpdate( creditCard._id, updateData, { new :true } );
		return res.status( 200 ).json( { updateCreditCard: updateCreditCard } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

const deleteCreditCard = async( req, res )=>{
	const creditCard = req.creditCard;

	try {
		const deleteCreditCard = await CreditCard.findByIdAndDelete( creditCard._id );
		return res.status( 200 ).json( { deleteCreditCard: deleteCreditCard } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};

module.exports.createCreditCard = createCreditCard;
module.exports.getCreditCard = getCreditCard;
module.exports.getCreditCards = getCreditCards;
module.exports.updateCreditCard = updateCreditCard;
module.exports.deleteCreditCard = deleteCreditCard;