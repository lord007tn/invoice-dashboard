const Invoice = require( "../models/invoice.models" );
const PaymentReceived = require( "../models/paymentReceived.models" );
const CertifiedScales = require( "../models/certifiedScales.models" );
const Scales = require( "../models/scales.models" );
const CigarettesCount = require( "../models/cigarettesCount.models" );
const OtherNote = require( "../models/otherNote.models" );
const createInvoice = async ( req, res )=>{
	const paymentsReceived = [];
	for ( let i = 0; i < req.body.paymentsReceived.length; i++ ) {
		const paymentReceived = new PaymentReceived( {
			name: req.body.paymentsReceived[i].name,
			amount: req.body.paymentsReceived[i].amount
		} );
		paymentsReceived.push( paymentReceived );
	}
	const certifiedScales = new CertifiedScales( {
		certifiedTicketStart: req.body.certifiedScales.certifiedTicketStart,
		certifiedTicketEnd: req.body.certifiedScales.certifiedTicketEnd,
		totalCertifiedScales: req.body.certifiedScales.totalCertifiedScales
	} );

	const scales = new Scales( {
		axlesWeightTicketStart: req.body.scales.axlesWeightTicketStart,
		axlesWeightTicketEnd: req.body.scales.axlesWeightTicketEnd,
		firstWeight: req.body.scales.firstWeight,
		reWeight: req.body.scales.reWeight
	} );

	const cigarettesCount = new CigarettesCount( {
		begin: req.body.cigarettesCount.begin,
		end: req.body.cigarettesCount.end,
		added: req.body.cigarettesCount.added,
		sold: req.body.cigarettesCount.sold
	} );

	const otherNotes = [];
	for ( let i = 0; i < req.body.otherNotes.length; i++ ) {
		const otherNote = new OtherNote( {
			note: req.body.otherNotes[i].note
		} );
		otherNotes.push( otherNote );
	}
	const invoice = new Invoice( {
		date: req.body.FormData.date,
		cashier: req.body.FormData.cashier,
		shift: req.body.FormData.shift,
		pos: req.body.FormData.pos,
		safeDrops: req.body.safeDrops,
		creditCards: req.body.creditCards,
		refunds: req.body.FormData.refunds,
		cashPaidOut: req.body.cashPaidOut,
		paymentReceived: paymentsReceived,
		certifiedScales: certifiedScales,
		scales: scales,
		cigarettesCount: cigarettesCount,
		otherNotes: otherNotes
	} );
	try {
		const savedInvoice = await invoice.save();
		return res.status( 200 ).json( savedInvoice );
	} catch ( err ) {
		return res.status( 500 ).json( err );
	}
};

const getInvoices = async ( req, res ) => {
	try {
		const invoices = await Invoice.find();
		return res.status( 200 ).json( { invoices: invoices } );
	} catch ( err ) {
		return res.status( 500 ).json( { err_message: err } );
	}
};
module.exports.createInvoice = createInvoice;
module.exports.getInvoices = getInvoices;